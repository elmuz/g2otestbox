##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Release
ProjectName            :=g2oTestBox
ConfigurationName      :=Release
WorkspacePath          := "/home/alessio/Sandbox/Tesi"
ProjectPath            := "/home/alessio/Sandbox/Tesi/g2oTestBox"
IntermediateDirectory  :=./Release
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=
Date                   :=06/04/16
CodeLitePath           :="/home/alessio/.codelite"
LinkerName             :=/usr/bin/g++
SharedObjectLinkerName :=/usr/bin/g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=$(PreprocessorSwitch)NDEBUG 
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="g2oTestBox.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)../g2o $(IncludeSwitch)/usr/include/eigen3 $(IncludeSwitch)/usr/local/include/opencv 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)opencv_calib3d $(LibrarySwitch)opencv_core $(LibrarySwitch)opencv_features2d $(LibrarySwitch)opencv_flann $(LibrarySwitch)opencv_highgui $(LibrarySwitch)opencv_imgcodecs $(LibrarySwitch)opencv_imgproc $(LibrarySwitch)opencv_ml $(LibrarySwitch)opencv_objdetect $(LibrarySwitch)opencv_photo $(LibrarySwitch)opencv_shape $(LibrarySwitch)opencv_stitching $(LibrarySwitch)opencv_superres $(LibrarySwitch)opencv_ts $(LibrarySwitch)opencv_video $(LibrarySwitch)opencv_videoio $(LibrarySwitch)opencv_videostab $(LibrarySwitch)opencv_xfeatures2d $(LibrarySwitch)g2o_cli $(LibrarySwitch)g2o_core $(LibrarySwitch)g2o_ext_freeglut_minimal $(LibrarySwitch)g2o_incremental $(LibrarySwitch)g2o_interactive $(LibrarySwitch)g2o_interface $(LibrarySwitch)g2o_opengl_helper $(LibrarySwitch)g2o_parser $(LibrarySwitch)g2o_simulator $(LibrarySwitch)cholmod $(LibrarySwitch)g2o_solver_cholmod $(LibrarySwitch)cxsparse $(LibrarySwitch)g2o_csparse_extension $(LibrarySwitch)g2o_solver_csparse $(LibrarySwitch)g2o_solver_dense $(LibrarySwitch)g2o_solver_eigen $(LibrarySwitch)g2o_solver_pcg $(LibrarySwitch)g2o_solver_structure_only $(LibrarySwitch)g2o_types_data $(LibrarySwitch)g2o_types_icp $(LibrarySwitch)g2o_types_sba $(LibrarySwitch)g2o_types_sclam2d $(LibrarySwitch)g2o_types_sim3 $(LibrarySwitch)g2o_types_slam2d $(LibrarySwitch)g2o_types_slam2d_addons $(LibrarySwitch)g2o_types_slam3d $(LibrarySwitch)g2o_types_slam3d_addons $(LibrarySwitch)g2o_viewer $(LibrarySwitch)g2o_stuff 
ArLibs                 :=  "opencv_calib3d" "opencv_core" "opencv_features2d" "opencv_flann" "opencv_highgui" "opencv_imgcodecs" "opencv_imgproc" "opencv_ml" "opencv_objdetect" "opencv_photo" "opencv_shape" "opencv_stitching" "opencv_superres" "opencv_ts" "opencv_video" "opencv_videoio" "opencv_videostab" "opencv_xfeatures2d" "g2o_cli" "g2o_core" "g2o_ext_freeglut_minimal" "g2o_incremental" "g2o_interactive" "g2o_interface" "g2o_opengl_helper" "g2o_parser" "g2o_simulator" "cholmod" "g2o_solver_cholmod" "cxsparse" "g2o_csparse_extension" "g2o_solver_csparse" "g2o_solver_dense" "g2o_solver_eigen" "g2o_solver_pcg" "g2o_solver_structure_only" "g2o_types_data" "g2o_types_icp" "g2o_types_sba" "g2o_types_sclam2d" "g2o_types_sim3" "g2o_types_slam2d" "g2o_types_slam2d_addons" "g2o_types_slam3d" "g2o_types_slam3d_addons" "g2o_viewer" "g2o_stuff" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)../g2o/lib $(LibraryPathSwitch)/usr/local/lib $(LibraryPathSwitch)/usr/lib 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++
CC       := /usr/bin/gcc
CXXFLAGS :=  -O2 -w -std=c++11 $(Preprocessors)
CFLAGS   :=   $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/src_main.cpp$(ObjectSuffix) $(IntermediateDirectory)/OpenMvgParser_OpenMvgParser.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@test -d ./Release || $(MakeDirCommand) ./Release


$(IntermediateDirectory)/.d:
	@test -d ./Release || $(MakeDirCommand) ./Release

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/src_main.cpp$(ObjectSuffix): src/main.cpp $(IntermediateDirectory)/src_main.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/alessio/Sandbox/Tesi/g2oTestBox/src/main.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_main.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_main.cpp$(DependSuffix): src/main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_main.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_main.cpp$(DependSuffix) -MM "src/main.cpp"

$(IntermediateDirectory)/src_main.cpp$(PreprocessSuffix): src/main.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_main.cpp$(PreprocessSuffix) "src/main.cpp"

$(IntermediateDirectory)/OpenMvgParser_OpenMvgParser.cpp$(ObjectSuffix): src/OpenMvgParser/OpenMvgParser.cpp $(IntermediateDirectory)/OpenMvgParser_OpenMvgParser.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/alessio/Sandbox/Tesi/g2oTestBox/src/OpenMvgParser/OpenMvgParser.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/OpenMvgParser_OpenMvgParser.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/OpenMvgParser_OpenMvgParser.cpp$(DependSuffix): src/OpenMvgParser/OpenMvgParser.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/OpenMvgParser_OpenMvgParser.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/OpenMvgParser_OpenMvgParser.cpp$(DependSuffix) -MM "src/OpenMvgParser/OpenMvgParser.cpp"

$(IntermediateDirectory)/OpenMvgParser_OpenMvgParser.cpp$(PreprocessSuffix): src/OpenMvgParser/OpenMvgParser.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/OpenMvgParser_OpenMvgParser.cpp$(PreprocessSuffix) "src/OpenMvgParser/OpenMvgParser.cpp"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./Release/


