/*
 * SfMParser.h
 *
 *  Created on: 16 mar 2016
 *      Author: andrea
 */

#ifndef SFMPARSER_H_
#define SFMPARSER_H_

#include <vector>
#include <Eigen/Core>

struct CameraType {
  Eigen::Matrix3d intrinsics;
  Eigen::Matrix3d rotation;
  Eigen::Vector3d translation;
  Eigen::Matrix4d cameraMatrix;
  Eigen::Vector3d center;
  Eigen::Matrix4d mvp;

  std::string pathImage;
  std::string fileName;

  int imageWidth;
  int imageHeight;

  std::vector<int> visiblePoints;
};

struct SfMData {

  int numPoints_;
  int numCameras_;

  std::vector<Eigen::Vector3d> points_;
  std::vector<CameraType> camerasList_;
  std::vector<std::string> camerasPaths_;

  std::vector<std::vector<int> > camViewingPointN_;
  std::vector<std::vector<int> > pointsVisibleFromCamN_;
  std::vector<std::vector<Eigen::Vector2d> > point2DoncamViewingPoint_;
  
  int imageWidth_, imageHeight_;
};

#endif /* CAM_PARSERS_SFMPARSER_H_ */
