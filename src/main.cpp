#include <stdio.h>
#include <unistd.h>
#include <random>
#include <math.h>
//#include <iostream>
//#include <fstream>
#include <Eigen/Core>
#include <Eigen/StdVector>
#include <Eigen/Geometry>
#include <opencv2/core.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/core/eigen.hpp>
#include "g2o/config.h"
#include "g2o/core/base_vertex.h"
#include "g2o/core/sparse_optimizer.h"
#include "g2o/core/block_solver.h"
#include "g2o/core/factory.h"
#include "g2o/core/hyper_graph_action.h"
#include "g2o/core/solver.h"
#include "g2o/core/robust_kernel_impl.h"
#include "g2o/core/optimization_algorithm_levenberg.h"
#include "g2o/solvers/dense/linear_solver_dense.h"
#include "g2o/types/sba/types_six_dof_expmap.h"
#include "g2o/types/sba/types_sba.h"
#include "g2o/types/sba/sbacam.h"
#include "g2o/solvers/structure_only/structure_only_solver.h"
#include "helper.h"
#include "OpenMvgParser/OpenMvgParser.h"
#include "g2o/solvers/cholmod/linear_solver_cholmod.h"
#include "g2o/solvers/csparse/linear_solver_csparse.h"


void print_usage(std::string program) {
  std::cout << program << " -i <sfm_data.json> -d <images folder> -n <num_iterations> [-b <noise>] [-k <kernel size>] [-R <R threshold>]" << std::endl;
}


int main(int argc, char *argv[]) {
  int opt;
  int flag_json = 0, flag_iter = 0, flag_path = 0;

  std::string sfm_data, img_path;
  int num_iter = 0;
  double noise = 0;
  int kernel_size = 3;
  double R_thres = -10000;
  bool edge_optim = true;

  while ((opt = getopt(argc, argv, "i:d:n:b:k:R:")) != -1) {
    switch (opt) {
      case 'i':
        flag_json = 1;
        sfm_data = optarg;
        std::cout << "SfM data: " << sfm_data << std::endl;
        break;
      case 'd':
        flag_path = 1;
        img_path = optarg;
        std::cout << "Pics folder: " << img_path << std::endl;
        break;
      case 'n':
        flag_iter = 1;
        num_iter = std::stoi(optarg);
        std::cout << "Number of iterations: " << num_iter << std::endl;
        break;
      case 'b':
        noise = std::stod(optarg);
        std::cout << "Noise level: " << noise << std::endl;
        break;
      case 'k':
        kernel_size = std::stoi(optarg);
        if (kernel_size < 1 || kernel_size % 2 == 0) {
          std::cout << "Kernel size must be odd and positive. A default value will be taken." << std::endl;
          kernel_size = 3;
        }
        if (kernel_size == 1) {
          edge_optim = false;
          std::cout << "EDGE OPTIMIZATION DISCARDED" << std::endl;
        } else {
          std::cout << "Kernel size: " << kernel_size << std::endl;
        }
        break;
      case 'R':
        R_thres = std::stod(optarg);
        if (edge_optim == true)
          std::cout << "R threshold for edge detection: " << R_thres << std::endl;
        break;
      default:
        print_usage(argv[0]);
        std::cout << "opt: " << opt << ", arg: " << optarg << ", argv[8]: " << argv[8];
        exit(EXIT_FAILURE);
    }
  }
  if (flag_json * flag_iter * flag_path != 1) {
    print_usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  // Import json file
  OpenMvgParser op(sfm_data);
  op.parse();

  // Import edges infos
  std::vector<cv::Mat> I_xx, I_xy, I_yy, M_xx, M_xy, M_yy;
  I_xx.assign(op.getSfmData().numCameras_, cv::Mat::zeros(op.getSfmData().camerasList_[0].imageHeight, op.getSfmData().camerasList_[0].imageWidth, CV_32F));
  I_xy.assign(op.getSfmData().numCameras_, cv::Mat::zeros(op.getSfmData().camerasList_[0].imageHeight, op.getSfmData().camerasList_[0].imageWidth, CV_32F));
  I_yy.assign(op.getSfmData().numCameras_, cv::Mat::zeros(op.getSfmData().camerasList_[0].imageHeight, op.getSfmData().camerasList_[0].imageWidth, CV_32F));
  M_xx.assign(op.getSfmData().numCameras_, cv::Mat::ones(op.getSfmData().camerasList_[0].imageHeight, op.getSfmData().camerasList_[0].imageWidth, CV_32F));
  M_xy.assign(op.getSfmData().numCameras_, cv::Mat::zeros(op.getSfmData().camerasList_[0].imageHeight, op.getSfmData().camerasList_[0].imageWidth, CV_32F));
  M_yy.assign(op.getSfmData().numCameras_, cv::Mat::ones(op.getSfmData().camerasList_[0].imageHeight, op.getSfmData().camerasList_[0].imageWidth, CV_32F));

  if (edge_optim) {

    for (int cam = 0; cam < op.getSfmData().numCameras_; cam++) {
      std::cerr << "Reading " << img_path << "/" << op.getSfmData().camerasList_[cam].fileName << std::endl;
      cv::Mat img = cv::imread(img_path + "/" + op.getSfmData().camerasList_[cam].fileName);
      std::cerr << "Width: " << img.cols << ", height: " << img.rows << std::endl;
      std::cerr << "Applying gaussian filter..." << std::endl;
      cv::GaussianBlur(img, img, cv::Size(5, 5), 0, 0, cv::BORDER_REFLECT_101);
      std::cerr << "Converting colorspace..." << std::endl;
      cv::cvtColor(img, img, CV_BGR2GRAY);

      // Get edges
      cv::Mat sobel_x, sobel_y;
      std::cerr << "Horizontal edge filter..." << std::endl;
      cv::Sobel(img, sobel_x, CV_32F, 1, 0, 3, 1, 0, cv::BORDER_REFLECT_101);
      std::cerr << "Vertical edge filter..." << std::endl;
      cv::Sobel(img, sobel_y, CV_32F, 0, 1, 3, 1, 0, cv::BORDER_REFLECT_101);

      // Get I_xx, I_xy, I_yy
      cv::multiply(sobel_x, sobel_x, I_xx[cam]);
      cv::multiply(sobel_x, sobel_y, I_xy[cam]);
      cv::multiply(sobel_y, sobel_y, I_yy[cam]);

      // Gaussian filter
      cv::GaussianBlur(I_xx[cam], I_xx[cam], cv::Size(kernel_size, kernel_size), 0, 0, cv::BORDER_REFLECT_101);
      cv::GaussianBlur(I_xy[cam], I_xy[cam], cv::Size(kernel_size, kernel_size), 0, 0, cv::BORDER_REFLECT_101);
      cv::GaussianBlur(I_yy[cam], I_yy[cam], cv::Size(kernel_size, kernel_size), 0, 0, cv::BORDER_REFLECT_101);

      std::cerr << "Calculating edges..." << std::endl;
      for (int i = 1; i < img.rows - 1; i++) {
        for (int j = 1; j < img.cols - 1; j++) {
          Eigen::Matrix2f covariance;
          covariance << I_xx[cam].at<float>(i, j), I_xy[cam].at<float>(i, j),
                     I_xy[cam].at<float>(i, j), I_yy[cam].at<float>(i, j);
          Eigen::Matrix2f information;
          float det, tr;
          bool invertible;
          covariance.computeInverseWithCheck(information, invertible, 0.001);
          if (invertible) {
            det = covariance.determinant();
            tr = covariance.trace();
            double R = det - 0.05 * std::pow(tr, 2.);
            //std::cerr << "Det: " << det << ", Trace: " << tr << ", R: " << R << std::endl;
            if (R < R_thres) {
              //if (det < 10 && tr > 10) {
              information *= sqrt(det); // normalize, determinant is det of its inverse
              //std::cerr << "Cov: " << covariance << std::endl;
              //std::cerr << "Inf: " << information << std::endl;
              //std::cerr << "Det of Information: " << information.determinant() << std::endl;
              if (!isnan(information(0, 0))) { // I get some NaN
                M_xx[cam].at<float>(i, j) = information(0, 0);
              } else {
                //std::cerr << "M_xx: " << information(0, 0) << std::endl;
                //std::cerr << "Covariance: " << covariance << std::endl;
              }
              if (!isnan(information(0, 1))) {
                M_xy[cam].at<float>(i, j) = information(0, 1);
              } else {
                //std::cerr << "M_xy: " << information(0, 1) << std::endl;
                //std::cerr << "Covariance: " << covariance << std::endl;
              }
              if (!isnan(information(1, 1))) {
                M_yy[cam].at<float>(i, j) = information(1, 1);
              } else {
                //std::cerr << "M_yy: " << information(1, 1) << std::endl;
                //std::cerr << "Covariance: " << covariance << std::endl;
              }
            }
          }
        }
      }
    }
  }

  g2o::SparseOptimizer optimizer;
  optimizer.setVerbose(false);
  g2o::OptimizationAlgorithmLevenberg *solver;

  g2o::BlockSolver_6_3::LinearSolverType *linearSolver;
#if G2O_HAVE_CHOLMOD
  std::cout << "Cholmod found" << std::endl;
  linearSolver = new g2o::LinearSolverCholmod<g2o::BlockSolver_6_3::PoseMatrixType>();
#elif defined G2O_HAVE_CSPARSE
  std::cout << "CSparse found" << std::endl;
  linearSolver = new g2o::LinearSolverCSparse<g2o::BlockSolver_6_3::PoseMatrixType>();
#else
  std::cout << "Error: neither CSparse nor Cholmod are available" << std::enld;
#endif


  g2o::BlockSolver_6_3 *solver_ptr = new g2o::BlockSolver_6_3(linearSolver);
  solver = new g2o::OptimizationAlgorithmLevenberg(solver_ptr);
  optimizer.setAlgorithm(solver);
  /*
    g2o::BlockSolverX::LinearSolverType * linearSolver;
    linearSolver = new g2o::LinearSolverCSparse<g2o::BlockSolverX::PoseMatrixType>();
    g2o::BlockSolverX * solver_ptr = new g2o::BlockSolverX(linearSolver);
    solver = new g2o::OptimizationAlgorithmLevenberg(solver_ptr);
    optimizer.setAlgorithm(solver);
  */
  // Prepare variables for noisy perturbance
  std::random_device rd;
  std::mt19937 gen(rd());

  int cam_limit = 0;
  int point_limit = 0;
  double error = 0;
  double error_after = 0;
  int vertex_id = 0;
  int mod_Omega = 0;
  int std_Omega = 0;

  // Adding camera poses
  std::cout << "Adding camera poses to the hypergraph...." << std::endl;

  if (cam_limit == 0)
    cam_limit = (int) op.getSfmData().camerasList_.size();

  for (int i_poses = 0; i_poses < cam_limit; i_poses++) {

    g2o::VertexCam *pose =  new g2o::VertexCam;

    pose->setId(vertex_id);
    vertex_id++;

    // Camera extrinsic parameters initialization
    // Rotation
    Eigen::Matrix3d pose_rotation = op.getSfmData().camerasList_[i_poses].rotation; // this is camera-centric, used in P = K [R | t]

    // Translation
    Eigen::Vector3d pose_translation = op.getSfmData().camerasList_[i_poses].center;

    // Fix first and last vertex
    if (i_poses == 0 || i_poses == cam_limit - 1) {
      pose->setFixed(true);
    } else {
      // Add noise to non-fixed vertices
      std::uniform_real_distribution<> generator(-noise, noise);
      Eigen::Vector3d noise_3d(generator(gen), generator(gen), generator(gen));
      pose_translation += noise_3d;

      double roll = generator(gen);
      double yaw = generator(gen);
      double pitch = generator(gen);
      Eigen::AngleAxisd rollAngle(roll, Eigen::Vector3d::UnitZ());
      Eigen::AngleAxisd yawAngle(yaw, Eigen::Vector3d::UnitY());
      Eigen::AngleAxisd pitchAngle(pitch, Eigen::Vector3d::UnitX());
      Eigen::Quaterniond noise_R = rollAngle * yawAngle * pitchAngle;
      pose_rotation *= noise_R.toRotationMatrix();
    }
    //pose->setFixed(true);
    g2o::SBACam estimate(Eigen::Quaterniond(pose_rotation.transpose()), pose_translation); // SBACam wants world-centric R

    // Camera intrinsic parameters initialization
    double focal_length, pp_x, pp_y;
    focal_length = op.getSfmData().camerasList_[i_poses].intrinsics(0, 0);
    pp_x = op.getSfmData().camerasList_[i_poses].intrinsics(0, 2);
    pp_y = op.getSfmData().camerasList_[i_poses].intrinsics(1, 2);
    estimate.setKcam(focal_length, focal_length, pp_x, pp_y, 0.0);

    pose->setEstimate(estimate);
    // std::cerr << estimate << std::endl << std::endl;

    optimizer.addVertex(pose);
  }
  
  int camera_vertices = vertex_id;

  // Adding 3D world point initialization
  std::cout << "Adding 3D world points to the hypergraph..." << std::endl;
  if (point_limit == 0)
    point_limit = (int) op.getSfmData().points_.size();

  for (int i_point = 0; i_point < point_limit; i_point++) {

    // std::cout << "Processing point "<< i_point << "/" << point_limit - 1 << " (vertex " << vertex_id << ")" << std::endl;

    // Remove points that are far away
    Eigen::Vector3d test_coord = op.getSfmData().points_[i_point];
    //if (test_coord[1] < -20)
    //  continue;

    // Take only points which are present in the reduced number of poses
    int visible_in_N_cams = 0;
    // Controlla che il punto 'i_point' sia visibile in almeno 2 camere entro il 'cam_limit'
    for (int j = 0; j < (int) op.getSfmData().camViewingPointN_[i_point].size(); j++) {
      if (op.getSfmData().camViewingPointN_[i_point][j] < cam_limit)
        visible_in_N_cams++;
    }
    if (visible_in_N_cams >= 2) {

      // Create the Vertex
      g2o::VertexSBAPointXYZ *point = new g2o::VertexSBAPointXYZ();

      point->setId(vertex_id);
      vertex_id++;

      point->setMarginalized(true);

      // Take 3D point coordinates and add some noise
      Eigen::Vector3d coords = op.getSfmData().points_[i_point];
      std::uniform_real_distribution<> generator(-noise, noise);
      Eigen::Vector3d noise_3d(generator(gen), generator(gen), generator(gen));
      coords += noise_3d;


      // Set estimate to the point and add it to the hypergraph
      point->setEstimate(coords);
      optimizer.addVertex(point);

      // Adding edges to the hypergraph
      // Take only edges for which the point 'i_point' is seen by the cam 'j_pose' < cam_limit
      for (int j_pose = 0; j_pose < (int) op.getSfmData().camViewingPointN_[i_point].size(); j_pose++) {
        if (op.getSfmData().camViewingPointN_[i_point][j_pose] < cam_limit) {

          g2o::EdgeProjectP2MC *e = new g2o::EdgeProjectP2MC();
          Eigen::Vector2d measure(op.getSfmData().point2DoncamViewingPoint_[i_point][j_pose]);

          e->vertices() [0] = dynamic_cast<g2o::OptimizableGraph::Vertex*>(point);  // point
          e->vertices() [1] = dynamic_cast<g2o::OptimizableGraph::Vertex*>
                              (optimizer.vertices().find(op.getSfmData().camViewingPointN_[i_point][j_pose])->second);  // pose
          e->setMeasurement(measure);

          // Apply information matrix according to edges
          Eigen::Matrix2d Omega;
          Omega << M_xx[j_pose].at<float>((int) floor(measure[1]), (int) floor(measure[0])), M_xy[j_pose].at<float>((int) floor(measure[1]), (int) floor(measure[0])),
                M_xy[j_pose].at<float>((int) floor(measure[1]), (int) floor(measure[0])), M_yy[j_pose].at<float>((int) floor(measure[1]), (int) floor(measure[0]));

          // Track how many modified information matrices play a role
          if ((Omega - Eigen::Matrix2d::Identity()).norm() > 0.000001) {
            mod_Omega++;
          } else {
            std_Omega++;
          }

          e->setInformation(Omega);
          g2o::RobustKernelHuber* rk = new g2o::RobustKernelHuber;
          e->setRobustKernel(rk);
          optimizer.addEdge(e);


          // calculate projection
          const g2o::VertexCam *cam = static_cast<const g2o::VertexCam*>(e->vertices()[1]);
          const g2o::Vector3D &pt = point->estimate();
          g2o::Vector4D ppt(pt(0), pt(1), pt(2), 1.0);
          g2o::Vector3D p = cam->estimate().w2i * ppt;
          g2o::Vector2D perr;
          perr = p.head<2>() / p(2);

          // std::cerr << std::endl << "M (edge):" << std::endl << cam->estimate().w2n << std::endl;
          // std::cerr << "POINT " << pt.transpose() << std::endl;
          // std::cerr << "PROJ  " << p.transpose() << std::endl;
          // std::cerr << "CPROJ " << perr.transpose() << std::endl;
          // std::cerr << "MEAS  " << e->measurement().transpose() << std::endl;

          error += sqrt(pow(perr(0) - e->measurement()(0), 2) + pow(perr(1) - e->measurement()(1), 2));
        }
      }
    }
  }

  std::cout << "Standard edges: " << std_Omega << ", modified edges: " << mod_Omega << std::endl;
  std::cout << "Error: " << error << std::endl;

  optimizer.save("before.g2o");
  optimizer.setVerbose(true);
  std::cout << "Initialize optimization..." << std::endl;
  optimizer.initializeOptimization();
  std::cout << "Optimization initialized, optimizing..." << std::endl;
  optimizer.optimize(num_iter);
  std::cout << "done." << std::endl;
  optimizer.save("after.g2o");
  g2oToMeshPointOnly("before.g2o", "mesh_before.ply", vertex_id - camera_vertices);
  g2oToMesh("before.g2o", "mesh&cam_before.ply", vertex_id);
  g2oToMeshPointOnly("after.g2o", "mesh_after.ply", vertex_id - camera_vertices);
  g2oToMesh("after.g2o", "mesh&cam_after.ply", vertex_id);

  // Calculate if error has become lower
  for (std::set<g2o::HyperGraph::Edge*>::iterator k = optimizer.edges().begin(); k != optimizer.edges().end(); k++) {

    g2o::EdgeProjectP2MC *edge = static_cast<g2o::EdgeProjectP2MC*>(*k);

    // calculate the projection
    const g2o::VertexCam *cam = static_cast<const g2o::VertexCam*>(edge->vertices()[1]);
    const g2o::Vector3D &pt = static_cast<const g2o::VertexSBAPointXYZ*>(edge->vertices()[0])->estimate();
    g2o::Vector4D ppt(pt(0), pt(1), pt(2), 1.0);
    g2o::Vector3D p = cam->estimate().w2i * ppt;
    g2o::Vector2D perr;
    perr = p.head<2>() / p(2);

    error_after += sqrt(pow(perr(0) - edge->measurement()(0), 2) + pow(perr(1) - edge->measurement()(1), 2));
  }
  std::cout << "Error after: " << error_after << std::endl;

  optimizer.clear();

}
