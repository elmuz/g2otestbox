#include <stdio.h>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <vector>
#include <Eigen/Core>
#include "OpenMvgParser/SfMData.h"



std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems)
{
  std::stringstream ss(s);
  std::string item;
  while (std::getline(ss, item, delim)) {
    elems.push_back(item);
  }
  return elems;
}

std::vector<std::string> split(const std::string &s, char delim)
{
  std::vector<std::string> elems;
  split(s, delim, elems);
  return elems;
}

void g2oToMesh(const std::string &g2o_out, const std::string & mesh_out, const int &total_pts)
{
  std::ofstream out_file;
  out_file.open(mesh_out);

  // Write an header
  out_file << "ply\n";
  out_file << "format ascii 1.0\n";
  out_file << "element vertex " << total_pts << "\n";
  out_file << "property float x\n";
  out_file << "property float y\n";
  out_file << "property float z\n";
  out_file << "property uchar red\n";
  out_file << "property uchar green\n";
  out_file << "property uchar blue\n";
  out_file << "end_header\n";

  std::ifstream in_file(g2o_out);
  std::string line;

  while (std::getline(in_file, line)) {

    std::vector<std::string> mesh_pt = split(line, ' ');

    if (mesh_pt[0] == "VERTEX_CAM") {
      out_file << mesh_pt[2] << " " << mesh_pt[3] << " " << mesh_pt[4] << " " << "0 255 0\n";
    } else if (mesh_pt[0] == "VERTEX_XYZ") {
      out_file << mesh_pt[2] << " " << mesh_pt[3] << " " << mesh_pt[4] << " " << "255 255 255\n";
    }
  }

  out_file.close();
}

void g2oToMeshPointOnly(const std::string &g2o_out, const std::string & mesh_out, const int &total_pts)
{
  std::ofstream out_file;
  out_file.open(mesh_out);

  // Write an header
  out_file << "ply\n";
  out_file << "format ascii 1.0\n";
  out_file << "element vertex " << total_pts << "\n";
  out_file << "property float x\n";
  out_file << "property float y\n";
  out_file << "property float z\n";
  out_file << "property uchar red\n";
  out_file << "property uchar green\n";
  out_file << "property uchar blue\n";
  out_file << "end_header\n";

  std::ifstream in_file(g2o_out);
  std::string line;

  while (std::getline(in_file, line)) {

    std::vector<std::string> mesh_pt = split(line, ' ');

    if (mesh_pt[0] == "VERTEX_XYZ") {
      out_file << mesh_pt[2] << " " << mesh_pt[3] << " " << mesh_pt[4] << " " << "255 255 255\n";
    }
  }

  out_file.close();
}

void g2oToMatlab(const std::string &g2o_out, const std::string &matlab_out)
{
  std::ofstream out_file;
  out_file.open(matlab_out);

  std::ifstream in_file(g2o_out);
  std::string line;
  int cam_id = 0;
  bool hold_on = true;

  while (std::getline(in_file, line)) {

    std::vector<std::string> cam_pt = split(line, ' ');

    if (cam_pt[0] == "VERTEX_CAM") {
      out_file << "c" << cam_id << " = [" << cam_pt[2] << ", " << cam_pt[3] << ", " << cam_pt[4] << "]';\n";
      out_file << "q" << cam_id << " = [" << cam_pt[5] << ", " << cam_pt[6] << ", " << cam_pt[7] << ", " << cam_pt[8] << "];\n";
      out_file << "r" << cam_id << " = quat2rotm(q" << cam_id << ");\n";
      out_file << "m" << cam_id << " = [r" << cam_id << ", c" << cam_id << "; 0 0 0 1];\n";
      out_file << "drawframe(m" << cam_id << ");\n";
      cam_id++;
    } else if (cam_pt[0] == "VERTEX_XYZ") {
      out_file << "plot3(" << cam_pt[2] << ", " << cam_pt[3] << ", " << cam_pt[4] << ", 'ok');\n";
    }
    if (hold_on) {
      out_file << "hold on;\n";
      hold_on = false;
    }
  }

  out_file.close();
}

void SfMToMatlab(const std::vector<CameraType> &cameras, const std::vector<Eigen::Vector3d> &points, const std::string &matlab_out)
{
  std::ofstream out_file;
  out_file.open(matlab_out);

  bool hold_on = true;
  
  for (int i = 0; i < (int) cameras.size(); i++) {
    out_file << "c" << i << " = [" << cameras[i].center(0) << ", " << cameras[i].center(1) << ", " << cameras[i].center(2) << "]';\n";
    out_file << "r" << i << " = [" << cameras[i].rotation(0,0) << ", " << cameras[i].rotation(0,1) << ", " << cameras[i].rotation(0,2);
    out_file << "; " << cameras[i].rotation(1,0) << ", " << cameras[i].rotation(1,1) << ", " << cameras[i].rotation(1,2);
    out_file << "; " << cameras[i].rotation(2,0) << ", " << cameras[i].rotation(2,1) << ", " << cameras[i].rotation(2,2) << "]';\n";
    out_file << "m" << i << " = [r" << i << ", c" << i << "; 0 0 0 1];\n";
    out_file << "drawframe(m" << i << ");\n";
    if (hold_on) {
      out_file << "hold on;\n";
      hold_on = false;
    }
  }
  
  for (int i = 0; i < (int) points.size(); i++) {
    out_file << "plot3(" << points[i](0) << ", " << points[i](1) << ", " << points[i](2) << ", 'ok');\n";
  }
  
  out_file.close();
}
